package id.co.pkp.springbootkafkademo.service;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-kafka-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/26/23
 * Time: 06:24
 * To change this template use File | Settings | File Templates.
 */
@Service
public class KafkaConsumer {
    private final static String topic = "hendiTopic";
    private final static String groupId = "hendiGroup";
    public static List<String> messages = new ArrayList<>();

    @KafkaListener(topics = topic, groupId = groupId)
    public void listen(String message) {
        messages.add(message);
    }
}
